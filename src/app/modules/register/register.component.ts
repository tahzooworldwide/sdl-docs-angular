import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  name: string = '';
  email: string = '';
  secret: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  handleNameChange(e: Event) {}
  handleEmailChange(e: Event) {}
  handleSecretChange(e: Event) {}

}
