import { NgModule } from '@angular/core';
import { RouterModule, Routes, UrlSegment } from '@angular/router';
import { InfoPage } from './pages/info/info.page';
import { ViewerPage } from './pages/viewer/viewer.page';
import { PublicationComponent } from './publication.component';

const pageRouteMatcher = (url: UrlSegment[]) => {
  if ( 
    url.length === 3
    && url[0].path.match(/^[0-9]+$/gm)
    ) {
    return {
      consumed: url,
      posParams: {
        // publicationId: new UrlSegment(url[0].path, {}),
        pageId: new UrlSegment(url[0].path, {}),
        publicationSlug: new UrlSegment(url[1].path, {}),
        pageSlug: new UrlSegment(url[2].path, {})
      }
    };
  }
  return null;
}

const routes: Routes = [
  {
    path: '',
    component: PublicationComponent,
    children: [
      {
        path: "",
        component: InfoPage
      },
      {
        // path: ":pageId",
        matcher: pageRouteMatcher,
        component: ViewerPage
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicationRoutingModule { }
