import { Injectable } from '@angular/core';
// import { KvPair } from 'src/app/core/interfaces/kv-pair';
import { PublicationModule } from '../publication.module';

const LS_NOTES_PREFIX: string = "topicnotes::"

@Injectable({
  // providedIn: PublicationModule
  providedIn: 'root'
})
export class NotesService {

  constructor() { }

  // getAllNotes() :KvPair[]{
  //   return Object.entries(localStorage)
  //     .filter(([k, v]) => k.toString().startsWith(LS_NOTES_PREFIX))
  //     .map(([k, v]) => {
  //       return { key: k, value: JSON.parse(v) }
  //     })
  // }

  getNote(key: string) {
    return localStorage.getItem(LS_NOTES_PREFIX + key);
  }

}
