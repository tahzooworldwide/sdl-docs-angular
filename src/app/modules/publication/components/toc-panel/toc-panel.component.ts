import { Component, Input, OnInit } from '@angular/core';
import { Publication } from 'src/app/core/interfaces/publication';

@Component({
  selector: 'pub-toc-panel',
  templateUrl: './toc-panel.component.html',
  styleUrls: ['./toc-panel.component.scss']
})
export class TocPanelComponent implements OnInit {

  activeLink = 0

  @Input() publication?: Publication;
  @Input() toc?: any[];

  constructor() { }

  ngOnInit(): void {
  }

}
