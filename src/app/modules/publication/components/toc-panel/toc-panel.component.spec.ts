import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TocPanelComponent } from './toc-panel.component';

describe('TocPanelComponent', () => {
  let component: TocPanelComponent;
  let fixture: ComponentFixture<TocPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TocPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TocPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
