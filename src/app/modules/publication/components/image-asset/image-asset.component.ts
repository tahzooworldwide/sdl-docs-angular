import { Attribute, Component, OnInit } from '@angular/core';

@Component({
  selector: 'pub-image-asset',
  templateUrl: './image-asset.component.html',
  styleUrls: ['./image-asset.component.scss']
})
export class ImageAssetComponent implements OnInit {
  open: boolean = false;
  binaryUrl: string;

  constructor(
    @Attribute("src") public src: string,
    @Attribute("alt") public alt: string
  ) { 
    this.binaryUrl = src;
  }

  ngOnInit(): void {
  }

  setOpen(val: boolean) {
    this.open = val;
  }


}
