import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DitaContentComponent } from './dita-content.component';

describe('DitaContentComponent', () => {
  let component: DitaContentComponent;
  let fixture: ComponentFixture<DitaContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DitaContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DitaContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
