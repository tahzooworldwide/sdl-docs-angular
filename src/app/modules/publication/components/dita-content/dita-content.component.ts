import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'pub-dita-content',
  templateUrl: './dita-content.component.html',
  styleUrls: ['./dita-content.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DitaContentComponent implements OnInit {

  @Input() dita: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
