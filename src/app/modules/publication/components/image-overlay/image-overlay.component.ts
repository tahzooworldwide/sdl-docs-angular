import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'pub-image-overlay',
  templateUrl: './image-overlay.component.html',
  styleUrls: ['./image-overlay.component.scss']
})
export class ImageOverlayComponent implements OnInit {

  @Input() src: string = '';
  @Input() alt: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  handleClose() {
    console.log('ImageOverlayComponent::handleClose');
  }

  handleSaverClick(e: Event) {
    console.log('ImageOverlayComponent::handleSaverClick');
    e.preventDefault();
    e.stopPropagation();

    const a = document.createElement('a');
    a.setAttribute('style', 'display:none');
    a.href = this.src;
    a.download = this.alt;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }



}
