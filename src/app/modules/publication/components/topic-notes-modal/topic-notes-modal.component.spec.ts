import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicNotesModalComponent } from './topic-notes-modal.component';

describe('TopicNotesModalComponent', () => {
  let component: TopicNotesModalComponent;
  let fixture: ComponentFixture<TopicNotesModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopicNotesModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicNotesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
