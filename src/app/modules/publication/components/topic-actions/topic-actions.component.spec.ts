import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicActionsComponent } from './topic-actions.component';

describe('TopicActionsComponent', () => {
  let component: TopicActionsComponent;
  let fixture: ComponentFixture<TopicActionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopicActionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
