import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pub-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.scss']
})
export class SearchPanelComponent implements OnInit {

  query: string = '';
  filteredResults = [];

  constructor() { }

  ngOnInit(): void {
  }

  handleSubmit(e: Event) {
    e.preventDefault();
  }

  handleInput(e: Event) {
    this.query = (e.target as HTMLInputElement).value;
    console.log( (e.target as HTMLInputElement).value, this.query);
  }

}
