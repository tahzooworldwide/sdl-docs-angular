import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pub-notes-panel',
  templateUrl: './notes-panel.component.html',
  styleUrls: ['./notes-panel.component.scss']
})
export class NotesPanelComponent implements OnInit {
  
  notes = [];

  constructor() { }

  ngOnInit(): void {
  }
  
  handleRefreshClick() {
    console.log('refresh click');
  }
}
