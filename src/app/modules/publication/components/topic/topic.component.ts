import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'pub-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements OnInit {

  @Input() title: string = '';
  @Input() body: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
