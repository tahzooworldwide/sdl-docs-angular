export interface Note {
  id: string,
  title: string,
  body: string,
  create_at: Date,
  updated_at: Date
}
