export enum MenuEnum {
  NONE = 0,
  TOC,
  SEARCH,
  TOOLS,
  NOTES
}
