import { Component, OnDestroy, OnInit } from '@angular/core';
import { PUBLICATIONS } from 'src/app/core/mocks/mock-publications';
import { MenuEnum } from './enums/menu.enum';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Apollo, gql } from 'apollo-angular';

const TOC_GQL = gql`
query GetPubPagesForTOC($publicationId: Int!){
  publication(namespaceId: 2, publicationId: $publicationId ) {
    __typename
    creationDate
    customMetas {
      edges {
        node {
          id
          itemId
          key
          value
          valueType
        }
      }
    }
    id
    initialPublishDate
    itemId
    itemType
    lastPublishDate
    namespaceId
    owningPublicationId
    multimediaPath
    multimediaUrl
    publicationId
    publicationKey
    publicationPath
    publicationUrl
    title
    updatedDate
  }
  items (
    filter: {
      itemTypes: [PAGE],
      namespaceIds: [2],
      publicationIds: [$publicationId]
    }
  ) {
    edges {
      node {
        __typename
        customMetas {
          edges {
            node {
              key
              value
              valueType
            }
          }
        }
        title
        ... on Page {
          url
        }
      }
    }
  }
}
`;

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.scss']
})
export class PublicationComponent implements OnInit, OnDestroy {
  loading: boolean = true;
  data: any;

  publicationId: string;
  toc: any[] = [];

  menuTab: MenuEnum = MenuEnum.TOC;

  publication: any;


  private querySubscription?: Subscription;

  constructor(
    private route: ActivatedRoute,
    private apollo: Apollo
  ) {
    this.publicationId = this.route.snapshot.params.publicationId
   }

  ngOnInit(): void {   
     this.querySubscription = this.apollo.watchQuery<any>({
    query: TOC_GQL,
    variables: {
      publicationId: this.publicationId
    }
  })
    .valueChanges
    .subscribe(({ data, loading }) => {
      this.loading = loading;
      console.log(data);
      this.data = data;
      this.publication = data.publication;
      this.toc = data.items.edges.map((edge: any) => {
        const {title, url} = edge.node;
        return { title, url};
      });
      console.log(this.toc);
      // this.publications = data.publications.edges.map((edge: any) => {
      //   return {
      //     title: edge.node.title,
      //     publicationId: edge.node.publicationId,
      //     lastPublishDate: new Date(edge.node.lastPublishDate)
      //   } as Publication;
      // });
      // console.log(this.publications);
    });
  }



  ngOnDestroy() {
    if (this.querySubscription !== undefined) {
      this.querySubscription.unsubscribe();
    }
  }

  public get MenuEnum() {
    return MenuEnum;
  }

  setMenuTab(val: MenuEnum) {
    this.menuTab = val;
  }

  toggle(val: MenuEnum) {
    this.menuTab = val === this.menuTab ? MenuEnum.NONE : val;
  }

}
