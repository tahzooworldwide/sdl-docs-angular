import { Component, OnInit } from '@angular/core';
import { KvPair } from 'src/app/core/interfaces/kv-pair';
import { PUBLICATIONS } from 'src/app/core/mocks/mock-publications';

@Component({
  selector: 'pub-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss']
})
export class InfoPage implements OnInit {

  publication = PUBLICATIONS[0];

  constructor() { }

  ngOnInit(): void {
  }

  getAttributePairs(): KvPair[] {
    return []
  }
  getMetadataPairs(): KvPair[] {
    return []
  }

}
