import { Component, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { Subscription } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from '@apollo/client/utilities';

const TOPIC_GQL = gql`
query GetTopic($publicationId: Int!, $pageId: Int){
  page(namespaceId: 2, publicationId:$publicationId, pageId: $pageId, contextData: [
      {uri:"taf:tcdl:render:binaryLink:relative", type:BOOLEAN, value:"false"}  
  ]) {
      __typename
      creationDate
      customMetas {
        edges {
          node {
            id
            itemId
            key
            value
            valueType
          }
        }
      }
      id
      initialPublishDate
      itemId
      itemType
      lastPublishDate
      namespaceId
      owningPublicationId
      publicationId
      title
      updatedDate
      containerItems {
          __typename
          id
          title
          
          ... on ComponentPresentation {
              rawContent(renderContent: true) {
                data
              }
            }
          }
      content {
          __typename
          id
          type
      }
      fileName
      pageTemplate {
          id
          title
      }
      rawContent(renderContent: true) {
        id
        charSet
        content
        data
      }
      taxonomies {
      id
      title
      }
      url
  }
}
`;

@Component({
  selector: 'pub-viewer',
  templateUrl: './viewer.page.html',
  styleUrls: ['./viewer.page.scss']
})
export class ViewerPage implements OnInit {
  loading: boolean = true;
  data: any;

  publicationId: string;
  publicationSlug: string;
  pageId: string;
  pageSlug: string;

  title: string = '';
  body: string = '';

  private querySubscription?: Subscription;

  constructor(
    private route: ActivatedRoute,
    private apollo: Apollo
  ) {
    console.log(this.route.snapshot);
    this.publicationId = this.route.snapshot.parent?.params.publicationId;
    this.publicationSlug = this.route.snapshot.params.publicationSlug;
    this.pageId = this.route.snapshot.params.pageId;
    this.pageSlug = this.route.snapshot.params.pageSlug;
  }

  ngOnInit(): void {
    this.querySubscription = this.apollo.watchQuery<any>({
      query: TOPIC_GQL,
      variables: {
        publicationId: this.publicationId,
        pageId: this.pageId
      }
    })
      .valueChanges
      .subscribe(({ data, loading }) => {
        this.loading = loading;
        console.log("TOPIC", data);
        console.log("TOPIC:containerItems", data.page.containerItems[0].rawContent.data.Component.Fields);
        const {topicTitle, topicBody} = data.page.containerItems[0].rawContent.data.Component.Fields;
        console.log('Topic title and body', topicTitle, topicBody);
        this.title = topicTitle.Values[0];
        this.body = topicBody.Values[0];
        // this.

        // this.publications = data.publications.edges.map((edge: any) => {
        //   return {
        //     title: edge.node.title,
        //     publicationId: edge.node.publicationId,
        //     lastPublishDate: new Date(edge.node.lastPublishDate)
        //   } as Publication;
        // });
        // console.log(this.publications);
      });
  }

  ngOnDestroy() {
    if (this.querySubscription !== undefined) {
      this.querySubscription.unsubscribe();
    }
  }

}
