import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicationRoutingModule } from './publication-routing.module';
import { PublicationComponent } from './publication.component';
import { SharedModule } from '../shared/shared.module';
import { InfoPage } from './pages/info/info.page';
import { TocPanelComponent } from './components/toc-panel/toc-panel.component';
import { NotesPanelComponent } from './components/notes-panel/notes-panel.component';
import { SearchPanelComponent } from './components/search-panel/search-panel.component';
import { ToolsPanelComponent } from './components/tools-panel/tools-panel.component';
import { TopicComponent } from './components/topic/topic.component';
import { DitaContentComponent } from './components/dita-content/dita-content.component';
import { ImageAssetComponent } from './components/image-asset/image-asset.component';
import { ImageOverlayComponent } from './components/image-overlay/image-overlay.component';
import { ViewerPage } from './pages/viewer/viewer.page';
import { NotesService } from './services/notes.service';
import { TopicNotesModalComponent } from './components/topic-notes-modal/topic-notes-modal.component';
import { TopicActionsComponent } from './components/topic-actions/topic-actions.component';
import { TopicToolbarComponent } from './components/topic-toolbar/topic-toolbar.component';


@NgModule({
  declarations: [
    PublicationComponent,
    InfoPage,
    ViewerPage,
    TocPanelComponent,
    NotesPanelComponent,
    SearchPanelComponent,
    ToolsPanelComponent,
    TopicComponent,
    DitaContentComponent,
    ImageAssetComponent,
    ImageOverlayComponent,
    TopicNotesModalComponent,
    TopicActionsComponent,
    TopicToolbarComponent,
  ],
  providers:[NotesService],
  imports: [
    CommonModule,
    PublicationRoutingModule,
    SharedModule
  ]
})
export class PublicationModule { }
