import { Component, OnDestroy, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { Subscription } from 'rxjs';
import { Publication } from 'src/app/core/interfaces/publication';

const PUBLICATIONS_GQL = gql`
query GetPublications($namespaceId: Int!, $first: Int) {
    publications(namespaceId: $namespaceId, first: $first)
    {
      edges
      {
        node{
          lastPublishDate
          publicationId
          title
        }
      }
    }
  }
`;

// type Edge = {
//   node: Publication
// }

// type Wrapper = {
//   edges: Edge[]
// }

// type Response = {
//   publications: Wrapper
// }


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  loading: boolean = true;
  data: any;

  publications: Publication[] = [];

  private querySubscription?: Subscription;

  constructor(
    private apollo: Apollo
  ) { }

  ngOnInit(): void {
    this.querySubscription = this.apollo.watchQuery<any>({
      query: PUBLICATIONS_GQL,
      variables: {
        namespaceId: 2,
        first: 100
      }
    })
      .valueChanges
      .subscribe(({ data, loading }) => {
        this.loading = loading;
        console.log(data);
        this.publications = data.publications.edges.map((edge: any) => {
          return {
            title: edge.node.title,
            publicationId: edge.node.publicationId,
            lastPublishDate: new Date(edge.node.lastPublishDate)
          } as Publication;
        });
        console.log(this.publications);
      });
  }

  ngOnDestroy() {
    if (this.querySubscription !== undefined) {
      this.querySubscription.unsubscribe();
    }
  }

}
