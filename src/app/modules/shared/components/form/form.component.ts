import { Component, Attribute, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor(
    @Attribute('type') public action?: string,
    @Attribute('type') public method: string = "GET"
  ) { }

  ngOnInit(): void {
  }

  onSubmit(e: Event) {
    e.preventDefault();
  }

}
