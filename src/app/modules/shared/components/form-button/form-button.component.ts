import { Component, Attribute, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-button',
  templateUrl: './form-button.component.html',
  styleUrls: ['./form-button.component.scss']
})
export class FormButtonComponent implements OnInit {

  constructor(@Attribute('type') public type: string) { }

  ngOnInit(): void {
  }

  onClick(e: Event) {
    e.preventDefault();
  }

}
