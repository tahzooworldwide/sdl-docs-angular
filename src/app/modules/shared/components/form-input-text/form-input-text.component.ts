import { Component, Attribute, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-input-text',
  templateUrl: './form-input-text.component.html',
  styleUrls: ['./form-input-text.component.scss']
})
export class FormInputTextComponent implements OnInit {
  value: any;
  disabled: string = 'false';

  constructor(
    @Attribute('id') public id?: string,
    @Attribute('name') public name?: string,
    @Attribute('type') public type: string = 'text',
    @Attribute('required') public required: string = 'false',
    @Attribute('autofocus') public autofocus: string = 'false',
    @Attribute('placeholder') public placeholder: string = '',
  ) { }

  ngOnInit(): void {
  }

  onChange(e: Event) {
    console.log(e);
  }
}
