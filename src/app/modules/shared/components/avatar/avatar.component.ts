import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {
  name: string = "Firstan Lasticus";
  initials: string = "FL";

  constructor() { }

  ngOnInit(): void {
  }

}
