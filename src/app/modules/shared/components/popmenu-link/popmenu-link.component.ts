import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popmenu-link',
  templateUrl: './popmenu-link.component.html',
  styleUrls: ['./popmenu-link.component.scss']
})
export class PopmenuLinkComponent implements OnInit {
  to: string = "#";

  constructor() { }

  ngOnInit(): void {
  }

}
