import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopmenuLinkComponent } from './popmenu-link.component';

describe('PopmenuLinkComponent', () => {
  let component: PopmenuLinkComponent;
  let fixture: ComponentFixture<PopmenuLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopmenuLinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopmenuLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
