import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-dropdown',
  templateUrl: './user-dropdown.component.html',
  styleUrls: ['./user-dropdown.component.scss']
})
export class UserDropdownComponent implements OnInit {
  isAuthenticated: boolean = false;

  // TODO: Add Avatar component as Dropdown Trigger

  constructor() { }

  ngOnInit(): void {
  }

}
