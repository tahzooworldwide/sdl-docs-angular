import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {
  open: boolean = false;

  // TODO: Need to pass in trigger component
  trigger?: any;
  
  constructor() { }

  ngOnInit(): void {
  }

  handleToggle() {
    this.open = !this.open;
  }

  handleClose() {
    this.open = false;
  }
}
