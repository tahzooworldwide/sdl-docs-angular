import { Component, Input, OnInit } from '@angular/core';
import { KvPair } from 'src/app/core/interfaces/kv-pair';

@Component({
  selector: 'app-key-value-table',
  templateUrl: './key-value-table.component.html',
  styleUrls: ['./key-value-table.component.scss']
})
export class KeyValueTableComponent implements OnInit {

  @Input() pairs: KvPair[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
