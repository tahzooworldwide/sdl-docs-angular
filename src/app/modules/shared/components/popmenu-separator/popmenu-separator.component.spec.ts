import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopmenuSeparatorComponent } from './popmenu-separator.component';

describe('PopmenuSeparatorComponent', () => {
  let component: PopmenuSeparatorComponent;
  let fixture: ComponentFixture<PopmenuSeparatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopmenuSeparatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopmenuSeparatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
