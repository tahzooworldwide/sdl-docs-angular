import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-site-header',
  templateUrl: './site-header.component.html',
  styleUrls: ['./site-header.component.scss']
})
export class SiteHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  handleBrandClick() {
    // TODO: Add call to routing to direct user to the root of the site/app
  }

}
