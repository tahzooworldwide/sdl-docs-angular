import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './components/panel/panel.component';
import { PanelListComponent } from './components/panel-list/panel-list.component';
import { PanelContentComponent } from './components/panel-content/panel-content.component';
import { AvatarComponent } from './components/avatar/avatar.component';
import { LoadingComponent } from './components/loading/loading.component';
import { PopmenuComponent } from './components/popmenu/popmenu.component';
import { PopmenuLinkComponent } from './components/popmenu-link/popmenu-link.component';
import { PopmenuSeparatorComponent } from './components/popmenu-separator/popmenu-separator.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { FormComponent } from './components/form/form.component';
import { FormButtonComponent } from './components/form-button/form-button.component';
import { FormGroupComponent } from './components/form-group/form-group.component';
import { FormInputTextComponent } from './components/form-input-text/form-input-text.component';
import { FormLabelComponent } from './components/form-label/form-label.component';
import { LayoutComponent } from './layouts/layout/layout.component';
import { SiteLayoutComponent } from './layouts/site-layout/site-layout.component';
import { SiteHeaderComponent } from './components/site-header/site-header.component';
import { UserDropdownComponent } from './components/user-dropdown/user-dropdown.component';
import { KeyValueTableComponent } from './components/key-value-table/key-value-table.component';



@NgModule({
  declarations: [
    PanelComponent,
    PanelListComponent,
    PanelContentComponent,
    AvatarComponent,
    LoadingComponent,
    PopmenuComponent,
    PopmenuLinkComponent,
    PopmenuSeparatorComponent,
    DropdownComponent,
    FormComponent,
    FormButtonComponent,
    FormGroupComponent,
    FormInputTextComponent,
    FormLabelComponent,
    LayoutComponent,
    SiteLayoutComponent,
    SiteHeaderComponent,
    UserDropdownComponent,
    KeyValueTableComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PanelComponent,
    PanelListComponent,
    PanelContentComponent,
    AvatarComponent,
    LoadingComponent,
    PopmenuComponent,
    PopmenuLinkComponent,
    PopmenuSeparatorComponent,
    DropdownComponent,
    FormComponent,
    FormButtonComponent,
    FormGroupComponent,
    FormInputTextComponent,
    FormLabelComponent,
    LayoutComponent,
    SiteLayoutComponent,
    SiteHeaderComponent,
    UserDropdownComponent,
    KeyValueTableComponent
  ]
})
export class SharedModule { }
