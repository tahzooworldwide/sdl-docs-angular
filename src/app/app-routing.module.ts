import { NgModule } from '@angular/core';
import { RouterModule, Routes, UrlSegment } from '@angular/router';

const publicationRouteMatcher = (url: UrlSegment[]) => {
  if ( url.length >= 1 && url[0].path.match(/^[0-9]+$/gm)) {
    return {
      consumed: url.slice(0, 1),
      posParams: {
        publicationId: new UrlSegment(url[0].path, {})
      }
    };
  }
  return null;
}

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule) },
  { path: 'login', loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule) },
  { path: 'register', loadChildren: () => import('./modules/register/register.module').then(m => m.RegisterModule) },
  { matcher: publicationRouteMatcher, loadChildren: () => import('./modules/publication/publication.module').then(m => m.PublicationModule) },
  // { matcher: pageRouteMatcher, loadChildren: () => import('./modules/publication/publication.module').then(m => m.PublicationModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
