import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from "../../../environments/environment";

interface TokenServiceResponse {
  access_token: string,
  refresh_token: string,
  token_type: string,
  expires_in: number
}


@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private token?: string;
  private refresh_token?: string;
  private token_type?: string;
  private expires_at: Date = new Date();

  constructor(
    private http: HttpClient
  ) { }

  getToken() {
    if (this.token !== undefined) {
      return this.token;
    }
    return this.requestToken();
  }

  private async requestToken() {

    const headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    let body = new URLSearchParams();
    body.set("client_id", environment.tokensvc.client_id);
    body.set("client_secret", environment.tokensvc.client_secret);
    body.set("grant_type", environment.tokensvc.grant_type);
    body.set("resources",  environment.tokensvc.resources);

    console.log(body.toString());

    const response = await this.http.post<TokenServiceResponse>('/authsvc', body.toString(), { headers: headers })
      .pipe(
        catchError(async e => console.log('token service', e))
      ).toPromise();

    console.log(response);
    if (response === undefined) {
      return undefined;
    } else {
      this.token = response.access_token;
      localStorage.setItem('token', this.token)
      return this.token;
    }
  }
}
