export interface User {
  name: string,
  initials: string,
  authenticated: boolean;
}
