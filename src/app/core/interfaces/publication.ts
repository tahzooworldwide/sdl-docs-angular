export interface Publication {
  publicationId: string,
  title: string,
  lastPublishDate: Date,
  attributes: object,
  metadata: []
}
