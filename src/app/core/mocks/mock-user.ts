import { User } from '../interfaces/user';

export const  USER: User = {
  name: 'Firstan Lasticus',
  initials: 'FL',
  authenticated: false
}

export const  AUTHENTICATED_USER: User = {
  name: 'Firstan Lasticus',
  initials: 'FL',
  authenticated: true
}