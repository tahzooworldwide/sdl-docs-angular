import { Publication } from '../interfaces/publication';

export const PUBLICATIONS: Publication[] = [
  {
    publicationId: 'pub1',
    title: 'Publication Name That Is Longer Than You Might Think. Stop Designing For 12-15 Characters',
    lastPublishDate: new Date(2020, 10, 13),
    attributes: {
      key1: "value1",
      key2: "value2"
    },
    metadata: []
  },
  {
    publicationId: 'pub2',
    title: 'Publication 2',
    lastPublishDate: new Date(2020, 11, 22),
    attributes: {
      key1: "value1",
      key2: "value2"
    },
    metadata: []
  },
  {
    publicationId: 'pub3',
    title: 'Publication 3',
    lastPublishDate: new Date(2021, 3, 15),
    attributes: {
      key1: "value1",
      key2: "value2"
    },
    metadata: []
  }
];