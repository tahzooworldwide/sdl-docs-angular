import { Component, OnInit } from '@angular/core';
import { TokenService } from './core/services/token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'faf';
  token?: string|undefined;

  constructor(
    private tokenService: TokenService
  ){}

  async ngOnInit() {
    const token = await this.tokenService.getToken();
    console.log(token);
  }
}
